from agents import *
import os, sys
import gymnasium as gym
import time
import text_flappy_bird_gym
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    env = gym.make('TextFlappyBird-v0', height = 15, width = 20, pipe_gap = 4)
    
    MC_agent_info = { "epsilon": 0.1,"env":env,"batch_size":100,"discount": 1}
    agent = MCAgent(MC_agent_info)
 
    agent.fit()
    obs ,_= env.reset()
    while True:

        # Select next action
        action = agent.policy(obs)  # for an agent, action = agent.policy(observation)

        # Appy action and return new observation of the environment
        obs, reward, done, _, info = env.step(action)

        # Render the game
        os.system("cls")
        sys.stdout.write(env.render())
        time.sleep(0.2) # FPS

        # If player is dead break
        if done:
            break

    env.close()



    

    # iterate
    # while True:

    #     # Select next action
    #     # action = env.action_space.sample()  # for an agent, action = agent.policy(observation)
    #     action = agent.policy(obs)
    #     # Appy action and return new observation of the environment
    #     obs, reward, done, _, info = env.step(action)

    #     # Render the game
    #     os.system("clear")
    #     sys.stdout.write(env.render())
    #     time.sleep(0.2) # FPS

    #     # If player is dead break
    #     if done:
    #         break

    # env.close()

