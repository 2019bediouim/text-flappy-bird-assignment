# Text Flappy Bird Assignment



## agents.py 

contains the Two implemented Agents MC and SARSA

## Assignment.ipynb

The notebook showcasing the results obtained with the agents for this environment

## main.py

Uses the MC Agent to play Flappy Bird

## Images
contains all the graphs displaying results

## IMPORTANT
PLOTLY was used in order to plot the graphs, since it render interactive plots in order to be able to see the plotted graphs on the notebook you need to install the library first in your coding environment : $\textit{pip install plotly}$
## Authors 
Melik Bedioui

