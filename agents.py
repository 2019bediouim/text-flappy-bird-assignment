import numpy as np 
from tqdm import tqdm
import plotly.graph_objects as go



class MCAgent():
    def __init__(self, agent_init_info):

        self.epsilon = agent_init_info["epsilon"]
        self.batch_size=agent_init_info["batch_size"]
        self.env=agent_init_info["env"]   
        self.gamma=agent_init_info["discount"]
    def map_space(self):
        self.map_dict={}
        cpt=0
        for i in range(self.env.observation_space[0].start,self.env.observation_space[0].n):
            for j in range(self.env.observation_space[1].start-1,self.env.observation_space[1].n):
                self.map_dict[(i,j)]=cpt
                cpt+=1
    
    def agent_init(self):
        self.map_space()
        self.Q =np.zeros((len(self.map_dict), self.env.action_space.n))   
        self.R =np.zeros((len(self.map_dict), self.env.action_space.n)) 
        self.N_samples =np.zeros((len(self.map_dict), self.env.action_space.n)) 

    def argmax(q_values):
        ties = np.argwhere(q_values == np.amax(q_values)).flatten()
        return np.random.choice(ties)
    
    def policy(self,obs):
        obs=self.map_dict[obs]
        return np.argmax(self.Q[obs,:])
   
    def agent_step(self,obs):
        obs=self.map_dict[obs]
        if np.random.rand()>self.epsilon:
            action=np.argmax(self.Q[obs,:])
        else : 
            action=self.env.action_space.sample()
        self.last_action=action
        return action
    
    def generate_episode(self):
        episode = []
        obs, _ = self.env.reset()
        reward_episode=0
        for _ in range(self.batch_size):
            action = self.agent_step(obs)
            new_obs, reward, done, _, _ = self.env.step(action)
            
            episode.append((obs, action, reward))
            reward_episode+=reward
            if done:
                break
            obs = new_obs
        return episode,reward_episode

    def fit(self,num_iterations,evaluate=False,N_test=1000,max_score=1000):
        self.agent_init()
        self.rewards=[]
        self.Score=[]
        self.Rew=[]
        self.num_ep=[]
        for j in tqdm(range(num_iterations)):
            
            episode,reward_episode = self.generate_episode()
            self.rewards.append(reward_episode)
            rewards,state_action=[],[]
            for i in range(len(episode)):
                rewards.append(episode[i][2])
                state_action.append((episode[i][0],episode[i][1]))
            
            for t, (o, a, _) in enumerate(episode):
                if not (o, a) in state_action[:t]:
                    o=self.map_dict[o]
                    self.R[o,a] += self.gamma*sum(rewards[t:])#we can add discount here
                    self.N_samples[o,a] += 1
                    self.Q[o,a] = (
                        self.R[o,a] / self.N_samples[o,a]
                    )
            if evaluate : 
                if (j<=5000 and j%100==0) or (j>5000 and j%1000==0):
                    self.num_ep.append(j)
                    scores=[]
                    rew=[]
                    for _ in range(N_test):
                        obs ,_= self.env.reset()
                        done=False
                        score=0
                        r=0
                        while True and score<max_score :
                            # Select next action
                            action = self.policy(obs)  
                            # Appy action and return new observation of the environment
                            obs, reward, done, _, info = self.env.step(action)
                            r+=reward
                            score=info["score"]
                            # If player is dead break
                            if done:
                                break
                        scores.append(score)
                        rew.append(r)
                    self.Score.append(np.mean(scores))
                    self.Rew.append(np.mean(rew))
 
    def test(self,epsilon=None,batch_size=None,N_train=5000,N_test=100,max_score=10000):
        if epsilon != None: 
            self.epsilon=epsilon
        if batch_size != None:
            self.batch_size=batch_size

        print("----Training the Agent----")
        self.fit(N_train)
        scores=[]
        print("----Testing the Agent----")
        for i in tqdm(range(N_test)):
            obs ,_= self.env.reset(seed=i)
            done=False
            score=0
            while True and score<max_score:
                # Select next action
                action = self.policy(obs)  
                # Appy action and return new observation of the environment
                obs, reward, done, _, info = self.env.step(action)
                score=info["score"]
                # If player is dead break
                if done:
                    break
            scores.append(score)

        self.env.close()
        
        return scores
       
    def plot_reward(self):
        fig=go.Figure()
        fig.add_trace(go.Scatter(x=np.arange(self.num_iterations),y=self.rewards,name="rewards during Train"))
        fig.update_layout(title="Rewards during Train for Monte-Carlo Agent",xaxis_title="Episode Number",yaxis_title="Rewards")
        fig.show()

class SARSAgent():
    def __init__(self, agent_init_info):
        
        self.epsilon = agent_init_info["epsilon"]
        self.alpha = agent_init_info["step_size"]
        self.gamma = agent_init_info["discount"]
        self.env=agent_init_info["env"]
        
    def map_space(self):
        self.map_dict={}
        cpt=0
        for i in range(self.env.observation_space[0].start,self.env.observation_space[0].n):
            for j in range(self.env.observation_space[1].start-1,self.env.observation_space[1].n):
                self.map_dict[(i,j)]=cpt
                cpt+=1

    def agent_init(self):
        self.map_space()
        self.Q =np.zeros((len(self.map_dict), self.env.action_space.n))   

    def argmax(q_values):
        ties = np.argwhere(q_values == np.amax(q_values)).flatten()
        return np.random.choice(ties)
    
    def agent_step(self,obs):
        if np.random.rand()>self.epsilon:
            action=np.argmax(self.Q[obs,:])
        else : 
            action=self.env.action_space.sample()
        return action
    
    def policy(self,obs):
        obs=self.map_dict[obs]
        return np.argmax(self.Q[obs,:])
    
    def Q_update(self,new_obs,new_action,reward):
        self.Q[self.last_state,self.last_action]+=\
        self.alpha*(reward+self.gamma*self.Q[new_obs, new_action]-self.Q[self.last_state,self.last_action]) 
        
    def fit(self,num_iterations,evaluate=False,N_test=1000,max_score=1000):
        self.agent_init()
        self.rewards=[]
        self.S=[]
        self.R=[]
        self.num_ep=[]
        for i in tqdm(range(num_iterations)):

            reward_episode=0
            done = False

            obs, _ = self.env.reset()
            obs= self.map_dict[obs]
            action = self.agent_step(obs)

            self.last_state=obs
            self.last_action=action

            while True:
                obs, reward, done, _, _ = self.env.step(action)
                obs=self.map_dict[obs]

                action = self.agent_step(obs)
                self.Q_update(obs,action,reward)

                reward_episode+=reward
                self.last_action=action
                self.last_state=obs
                if done:
                    self.rewards.append(reward_episode)
                    break
            if evaluate : 
                if (i<=5000 and i%100==0) or (i>5000 and i%1000==0):
                    self.num_ep.append(i)
                    scores=[]
                    rewards=[]
                    for _ in range(N_test):
                        obs ,_= self.env.reset()
                        done=False
                        score=0
                        r=0
                        while True and score<max_score :
                            # Select next action
                            action = self.policy(obs)  
                            # Appy action and return new observation of the environment
                            obs, reward, done, _, info = self.env.step(action)
                            r+=reward
                            score=info["score"]
                            # If player is dead break
                            if done:
                                break
                        scores.append(score)
                        rewards.append(r)
                    self.S.append(np.mean(scores))
                    self.R.append(np.mean(rewards))
                     
    def test(self,epsilon=None,alpha=None,N_train=5000,N_test=100,max_score=10000):
        if epsilon != None: 
            self.epsilon=epsilon
        if alpha != None:
            self.alpha=alpha
        print("----Training the Agent----")
        self.fit(N_train)
        scores=[]
        rewards=[]

        print("----Testing the Agent----")
        for _ in tqdm(range(N_test)):
            obs ,_= self.env.reset()
            done=False
            score=0
            r=0
            while True and score<max_score:
                # Select next action
                action = self.policy(obs)  
                # Appy action and return new observation of the environment
                obs, reward, done, _, info = self.env.step(action)
                r+=reward
                score=info["score"]
                # If player is dead break
                if done:
                    
                    break
            scores.append(score)
            rewards.append(r)
        self.env.close()
        return scores,rewards

    def plot_reward(self,num_iterations):
        fig=go.Figure()
        fig.add_trace(go.Scatter(x=np.arange(self.num_iterations),y=self.rewards,name="rewards during Train"))
        fig.update_layout(title="Rewards during Train for SARSA Agent",xaxis_title="Episode Number",yaxis_title="Rewards")
        fig.show()

